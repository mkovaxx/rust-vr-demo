extern crate cgmath;
use cgmath::Matrix;
use cgmath::SquareMatrix;
use cgmath::prelude::InnerSpace;

#[macro_use]
extern crate glium;
use glium::GlObject;
use glium::framebuffer::ToColorAttachment;
use glium::framebuffer::ToDepthStencilAttachment;

extern crate openvr;

fn main() {
    use glium::{glutin, Surface};

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new();
    let glutin_context = glutin::ContextBuilder::new();
    let display = glium::Display::new(window, glutin_context, &events_loop).unwrap();

    let openvr_context = match unsafe { openvr::init(openvr::ApplicationType::Scene) } {
        Ok(ivr) => ivr,
        Err(err) => {
            println!("Failed to initialize openvr: {}", err);
            return;
        }
    };

    println!("OpenVR was initialized successfully..");

    let system = match openvr_context.system() {
        Ok(sys) => sys,
        Err(err) => {
            println!("Failed to get system interface: {}", err);
            return;
        }
    };

    let compositor = match openvr_context.compositor() {
        Ok(ext) => ext,
        Err(err) => {
            println!("Failed to create IVRCompositor subsystem: {}", err);
            return;
        }
    };

    system.device_to_absolute_tracking_pose(
        openvr::TrackingUniverseOrigin::Standing,
        0.005,
    );

    let target_size = system.recommended_render_target_size();

    let left_color =
        glium::framebuffer::RenderBuffer::new(
            &display,
            glium::texture::UncompressedFloatFormat::U8U8U8U8,
            target_size.0,
            target_size.1,
        ).unwrap();
    let left_depth =
        glium::framebuffer::DepthStencilRenderBuffer::new(
            &display,
            glium::texture::DepthStencilFormat::I24I8,
            target_size.0,
            target_size.1,
        ).unwrap();
    let mut left_target =
        glium::framebuffer::SimpleFrameBuffer::with_depth_stencil_buffer(
            &display,
            left_color.to_color_attachment(),
            left_depth.to_depth_stencil_attachment(),
        ).unwrap();
    let left_projection =
        cgmath::Matrix4::from(system.projection_matrix(openvr::Eye::Left, 0.001f32, 100.0f32)).transpose();
    let left_eye_view =
        cgmath::Matrix4::from(pad_mat3x4_to_mat4x4(system.eye_to_head_transform(openvr::Eye::Left))).transpose().invert().unwrap();

    let right_color =
        glium::framebuffer::RenderBuffer::new(
            &display,
            glium::texture::UncompressedFloatFormat::U8U8U8U8,
            target_size.0,
            target_size.1,
        ).unwrap();
    let right_depth =
        glium::framebuffer::DepthStencilRenderBuffer::new(
            &display,
            glium::texture::DepthStencilFormat::I24I8,
            target_size.0,
            target_size.1,
        ).unwrap();
    let mut right_target =
        glium::framebuffer::SimpleFrameBuffer::with_depth_stencil_buffer(
            &display,
            right_color.to_color_attachment(),
            right_depth.to_depth_stencil_attachment(),
        ).unwrap();
    let right_projection =
        cgmath::Matrix4::from(system.projection_matrix(openvr::Eye::Right, 0.01f32, 100.0f32)).transpose();
    let right_eye_view =
        cgmath::Matrix4::from(pad_mat3x4_to_mat4x4(system.eye_to_head_transform(openvr::Eye::Right))).transpose().invert().unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 3],
    }

    implement_vertex!(Vertex, position);

    // make a wireframe cube
    let s = 0.125; // size in meters
    let vertices = vec![
        Vertex { position: [-s, -s, -s] },
        Vertex { position: [ s, -s, -s] },
        Vertex { position: [-s,  s, -s] },
        Vertex { position: [ s,  s, -s] },
        Vertex { position: [-s, -s,  s] },
        Vertex { position: [ s, -s,  s] },
        Vertex { position: [-s,  s,  s] },
        Vertex { position: [ s,  s,  s] },
    ];
    let indices: Vec<u32> = vec![
        0, 1,
        2, 3,
        0, 2,
        1, 3,
        0, 4,
        1, 5,
        2, 6,
        3, 7,
        4, 5,
        6, 7,
        4, 6,
        5, 7,
    ];

    let vertex_buffer = glium::VertexBuffer::new(&display, &vertices).unwrap();
    let index_buffer = glium::IndexBuffer::new(&display, glium::index::PrimitiveType::LinesList, &indices).unwrap();

    let vertex_shader_src = r#"
        #version 140

        in vec3 position;

        uniform mat4 projection;
        uniform mat4 eye_view;
        uniform mat4 head_view;
        uniform mat4 model_view;

        void main() {
            gl_Position = projection * eye_view * head_view * model_view * vec4(position, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 140

        out vec4 color;

        void main() {
            color = vec4(0.0, 1.0, 1.0, 1.0);
        }
    "#;

    let program = glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap();

    let mut running = true;
    while running {
        let mut target = display.draw();

        let poses = compositor.wait_get_poses().unwrap();

        let head_view =
            cgmath::Matrix4::from(pad_mat3x4_to_mat4x4(poses.render[0].device_to_absolute_tracking().clone())).transpose().invert().unwrap();

        let model_view =
            cgmath::Matrix4::from_translation(cgmath::Vector3 { x: 0.0, y: 1.5, z: 0.0 }) *
                cgmath::Matrix4::from_axis_angle(
                    cgmath::Vector3 { x: -1.0, y: 0.0, z: 1.0 }.normalize(),
                    cgmath::Deg(54.73)
                );

        let uniforms = uniform! {
            projection: <[[f32; 4]; 4]>::from(left_projection.into()),
            eye_view: <[[f32; 4]; 4]>::from(left_eye_view.into()),
            head_view: <[[f32; 4]; 4]>::from(head_view.into()),
            model_view: <[[f32; 4]; 4]>::from(model_view.into()),
        };

        let left_uniforms = uniform! {
            projection: <[[f32; 4]; 4]>::from(left_projection.into()),
            eye_view: <[[f32; 4]; 4]>::from(left_eye_view.into()),
            head_view: <[[f32; 4]; 4]>::from(head_view.into()),
            model_view: <[[f32; 4]; 4]>::from(model_view.into()),
        };

        let right_uniforms = uniform! {
            projection: <[[f32; 4]; 4]>::from(right_projection.into()),
            eye_view: <[[f32; 4]; 4]>::from(right_eye_view.into()),
            head_view: <[[f32; 4]; 4]>::from(head_view.into()),
            model_view: <[[f32; 4]; 4]>::from(model_view.into()),
        };

        target.clear_color(0.0, 0.0, 0.0, 1.0);
        target.draw(&vertex_buffer, &index_buffer, &program, &uniforms,
                    &Default::default()).unwrap();
        target.finish().unwrap();

        left_target.clear_color(0.0, 0.0, 0.0, 1.0);
        left_target.draw(&vertex_buffer, &index_buffer, &program, &left_uniforms,
                    &Default::default()).unwrap();

        right_target.clear_color(0.0, 0.0, 0.0, 1.0);
        right_target.draw(&vertex_buffer, &index_buffer, &program, &right_uniforms,
                    &Default::default()).unwrap();

        unsafe {
            let left_result = compositor.submit(
                openvr::Eye::Left,
                &openvr::compositor::texture::Texture {
                    handle: openvr::compositor::texture::Handle::OpenGLRenderBuffer(left_color.get_id() as usize),
                    color_space: openvr::compositor::texture::ColorSpace::Linear,
                },
                None,
                None,
            );
            match left_result {
                Ok(()) => (),
                Err(error) => println!("Compositor error: {}", error),
            }
            let right_result = compositor.submit(
                openvr::Eye::Right,
                &openvr::compositor::texture::Texture {
                    handle: openvr::compositor::texture::Handle::OpenGLRenderBuffer(right_color.get_id() as usize),
                    color_space: openvr::compositor::texture::ColorSpace::Linear,
                },
                None,
                None,
            );
            match right_result {
                Ok(()) => (),
                Err(error) => println!("Compositor error: {}", error),
            }
            compositor.post_present_handoff();
        }

        events_loop.poll_events(|event| {
            match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::Closed => running = false,
                    _ => (),
                },
                _ => (),
            }
        });
    }
}

fn pad_mat3x4_to_mat4x4(m: [[f32; 4]; 3]) -> [[f32; 4]; 4] {
    [
        m[0].clone(),
        m[1].clone(),
        m[2].clone(),
        [0.0, 0.0, 0.0, 1.0],
    ]
}
