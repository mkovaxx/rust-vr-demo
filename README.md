# Rust VR Demo

A minimal VR demo in Rust to help you get started.

It uses OpenVR (via [openvr](https://crates.io/crates/openvr)) to interface with the headset, and OpenGL (via [glium](https://crates.io/crates/glium)) to render.

It creates a neon blue wireframe cube that floats near the center of the play area.

## Prerequisites

- [Install Rust](https://www.rust-lang.org/en-US/install.html)

## Commands to Build or Run

```
cargo build
```

```
cargo run
```

## Why Rust

Low latency is a must for immersive VR experiences.
Rust offers low-cost abstractions, including robust memory management without a garbage collector.
It comes with a real module system and a package manager, which are two great advantages over C++.
These properties make Rust ideal for developing VR content.
